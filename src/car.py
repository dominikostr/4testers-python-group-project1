class Car:
    def __init__(self, model,  car_year, distance):
        self.model = model
        self.car_year = car_year
        self.distance = 0

    def drive(self, amount_of_distance):
        self.distance += amount_of_distance

    def has_warranty(self):
        if 2022 - self.car_year > 7 or self.distance > 120000:
            return False
        else:
            return True



    def get_description(self ):
        return f'This is a {self.model} made in {self.car_year}. Currently it drove {self.distance} kilometers'

if __name__ == '__main__':
    car1 = Car('Yaris', 2022, 0)
    print(car1.model)
    print(car1.car_year)
    print(car1.distance)

    car2 = Car('Ibiza', 2000, 0)
    print(car2.model)
    print(car2.car_year)
    print(car2.distance)

    car1.has_warranty()
    print(car1.has_warranty())

    car2.has_warranty()
    print(car2.has_warranty())

    car1.drive(100)
    print(car1.distance)

    car1.drive(100)
    print(car1.distance)

    car2.drive(100)
    print(car2.distance)

    print(car1.get_description())
    print(car2.get_description())



